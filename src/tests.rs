#[allow(unused_imports)]
use crate::common;
#[allow(unused_imports)]
use crate::common::Normalizable;
#[allow(unused_imports)]
use gl_matrix::common::*;
#[allow(unused_imports)]
use gl_matrix::mat4;

#[test]
fn test_matrix_converter() {
	let mat4x4 = [
		[1.0_f32, 0.0, 5.0, 0.0],
		[4.0_f32, 0.0, 8.0, 3.0],
		[3.0_f32, 7.0, 0.0, 2.0],
		[6.0_f32, 0.0, 1.0, 9.0],
	];
	let mat16 = [
		1.0_f32, 0.0, 5.0, 0.0,
		4.0_f32, 0.0, 8.0, 3.0,
		3.0_f32, 7.0, 0.0, 2.0,
		6.0_f32, 0.0, 1.0, 9.0,
	];
	assert_eq!(mat4x4, common::convert_matrix(&mat16));
}

#[test]
fn test_normalize() {
	let vec: Vec3 = [1.0, 0.0, 0.0];
	let mut n_vec: Vec3 = [1.0, 0.0, 0.0];
	n_vec.normalize();
	assert!(vec[0] == n_vec[0]);
	assert!(vec[1] == n_vec[1]);
	assert!(vec[2] == n_vec[2]);
}
