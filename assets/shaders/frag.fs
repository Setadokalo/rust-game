#version 330 core

in vec2 uv_f;
in vec3 normal;
in vec3 model_normal;

out vec4 color;

uniform float time;
uniform sampler2D albedo_texture;
uniform sampler2D normal_texture;

in vec3 light_dir_tanspace;
in vec3 eye_dir_tanspace;

const float frac_to_apply = 0.0;
const float strength = 0.95;

void main() {
	vec2 uv = uv_f;
	// uv.x += time * 0.1;
	vec3 n_light_vec = normalize(-light_dir_tanspace);
	vec3 n_eye_vec = normalize(eye_dir_tanspace);
	vec3 normal_d = normalize(texture(normal_texture, uv).rgb*2.0 - 1.0);
	
	float cosTheta = clamp(dot(normal_d, n_light_vec), 0,1);

	vec3 reflection_dir = normalize(reflect(vec3(-1), normal_d));
	float cosAlpha = clamp(dot(n_eye_vec, reflection_dir) - frac_to_apply, 0, 1);
	// cosTheta = 1.0 - ((1.0 - cosTheta) * scale);
	vec3 albedo = texture(albedo_texture, uv).rgb; 
	// vec3 albedo = vec3(0.6235, 0.8314, 1.0);
	float lit_factor = cosTheta + 0.03;
	color = vec4(albedo * lit_factor, 1.0);
	color.rgb += vec3(pow(cosAlpha, 256.0) * strength);
	// color.rgb = normal;
	// color.rgb = vec3(1.0, 0.0, 0.0);
}