use std::sync::mpsc;
use std::thread;

use gl_matrix::common::*;
use gl_matrix::{mat4, mat3, quat};
use glium::{implement_vertex, uniform, Surface};
use crate::file_loading;

// adding a new trait to the Vec* structs because how the fuck did the dev not think to add normalizing functions for vectors? SUPER common requirement
pub trait Normalizable {
	fn normalize(&mut self);
	fn normalized(&self) -> Self;
}
impl Normalizable for Vec2 {
	fn normalize(&mut self) {
		let length = (self[0].powi(2) + self[1].powi(2)).sqrt();
		self[0] = self[0]/length;
		self[1] = self[1]/length;
	}
	fn normalized(&self) -> Self {
		let mut new_vec = [0.0_f32; 2];
		let length = (self[0].powi(2) + self[1].powi(2)).sqrt();
		new_vec[0] = self[0]/length;
		new_vec[1] = self[1]/length;
		new_vec
	}
}
impl Normalizable for Vec3 {
	fn normalize(&mut self) {
		let length = (self[0].powi(2) + self[1].powi(2) + self[2].powi(2)).sqrt();
		self[0] = self[0]/length;
		self[1] = self[1]/length;
		self[2] = self[2]/length;
	}
	fn normalized(&self) -> Self {
		let mut new_vec = [0.0_f32; 3];
		let length = (self[0].powi(2) + self[1].powi(2) + self[2].powi(2)).sqrt();
		new_vec[0] = self[0]/length;
		new_vec[1] = self[1]/length;
		new_vec[2] = self[2]/length;
		new_vec
	}
}
impl Normalizable for Vec4 {
	fn normalize(&mut self) {
		let length = (self[0].powi(2) + self[1].powi(2) + self[2].powi(2) + self[3].powi(2)).sqrt();
		self[0] = self[0]/length;
		self[1] = self[1]/length;
		self[2] = self[2]/length;
		self[3] = self[3]/length;
	}
	fn normalized(&self) -> Self {
		let mut new_vec = [0.0_f32; 4];
		let length = (self[0].powi(2) + self[1].powi(2) + self[2].powi(2) + self[3].powi(2)).sqrt();
		new_vec[0] = self[0]/length;
		new_vec[1] = self[1]/length;
		new_vec[2] = self[2]/length;
		new_vec[3] = self[3]/length;
		new_vec
	}
}

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
	pub position: [f32; 3],
	pub uv: [f32; 2],
	pub vert_normal: [f32; 3],
	pub tangent: [f32; 3],
	pub bitangent: [f32; 3],
}
impl std::cmp::PartialEq for Vertex {
	// tangent and bitangent are ignored because they are merely properties for equivalent vertices to share
	fn eq(&self, other: &Self) -> bool {
		for i in 0..3 {
			if self.position[i] != other.position[i] {
				return false;
			}
			if self.vert_normal[i] != other.vert_normal[i] {
				return false;
			}
		}
		for i in 0..2 {
			if self.uv[i] != other.uv[i] {
				return false;
			}
		}
		return true;
	}
	
}
impl Vertex {
	pub fn new(position: Vec3, uv: Vec2, normal: Vec3, tangent: Vec3, bitangent: Vec3) -> Vertex {
		Vertex {
			position,
			uv,
			vert_normal: normal,
			tangent,
			bitangent
		}
	}
}
implement_vertex!(Vertex, position, uv, vert_normal, tangent, bitangent);


pub fn to_quaternion(yaw: f32, pitch: f32, roll: f32) -> Quat // yaw (Z), pitch (Y), roll (X)
{
	// Abbreviations for the various angular functions
	let cy = (yaw * 0.5).cos();
	let sy = (yaw * 0.5).sin();
	let cp = (pitch * 0.5).cos();
	let sp = (pitch * 0.5).sin();
	let cr = (roll * 0.5).cos();
	let sr = (roll * 0.5).sin();

	let mut q: Quat = Quat::default();
	q[0] = cr * cp * cy + sr * sp * sy;
	q[1] = sr * cp * cy - cr * sp * sy;
	q[2] = cr * sp * cy + sr * cp * sy;
	q[3] = cr * cp * sy - sr * sp * cy;
	
	q
}

pub fn get_transformation_matrix(scale: &Vec3, translation: &Vec3, rotation: &Vec3) -> Mat4 {
	let mut transform_matrix: Mat4 = mat4::create();
	let rot: Quat = to_quaternion(rotation[0], rotation[1], rotation[2]);
	mat4::from_rotation_translation_scale(&mut transform_matrix, &rot, translation, scale);

	transform_matrix
}
 
// Camera transforms are inverses of normal transforms
pub fn get_camera_transformation_matrix(translation: &Vec3, rotation: &Vec3) -> Mat4 {
	let mut transform_matrix: Mat4 = mat4::create();
	let translation = [translation[0], translation[1], translation[2]];
	mat4::from_translation(&mut transform_matrix, &translation); //, &rotation
	let mut rotation_matrix: Mat4 = mat4::create();
	mat4::from_rotation(&mut rotation_matrix, rotation[0], &[1.0, 0.0, 0.0]);
	rotation_matrix =
		// mat4::scale(&mut mat4::create(), 
			mat4::rotate_y(&mut mat4::create(), &rotation_matrix, rotation[1]);
			// &[-1.0, -1.0, 1.0]);
	mat4::multiply(&mut mat4::create(), &rotation_matrix, &transform_matrix)
}
 


// This converts a matrix from 1-dimensional 16 space array format (1x16) to 2 dimensional 4x4 representation. Glium can't take the 1x16 format
// and the best matrix library I could find uses 1x16
pub fn convert_matrix(matrix: &[f32; 16]) -> [[f32; 4]; 4] {
	let mut ret = [[0.0f32; 4]; 4];
	for i in 0..16 {
		ret[i/4][i%4] = matrix[i];
	}
	ret
}
// This converts a matrix from 1-dimensional 16 space array format (1x16) to 2 dimensional 4x4 representation. Glium can't take the 1x16 format
// and the best matrix library I could find uses 1x16
pub fn convert_matrix_3x3(matrix: &[f32; 9]) -> [[f32; 3]; 3] {
	let mut ret = [[0.0f32; 3]; 3];
	for i in 0..9 {
		ret[i/3][i%3] = matrix[i];
	}
	ret
}


// This struct stores and provides a texture that is loaded from the disk on a second thread. 
// For textures it is better to provide an empty texture than return a conditionally valid resource imo, so we return `empty_texture`
// when the code asks for the texture before it's ready
pub struct OffloadedTexture<'a> {
	empty_texture: glium::Texture2d,
	texture: Option<glium::Texture2d>,
	receiver: mpsc::Receiver<(Vec<u8>, (u32, u32))>,
	display: &'a glium::Display
}
impl<'a> OffloadedTexture<'a> {
	// Start a new thread with an anonymous function that loads the texture and then returns a message to the internal message pipeline
	pub fn new(path: String, disp: &'a glium::Display, empty_texture: glium::Texture2d) -> OffloadedTexture<'a> {
		let (sender, receiver) = mpsc::channel();

		let ret = OffloadedTexture {
			texture: None,
			receiver,
			display: disp,
			empty_texture
		};
		thread::spawn(move || {
			println!("[Texture] {}", path);
			sender.send(file_loading::load_texture(&path[..])).expect("Failed to send texture data to main thread!");
		});
		ret
	}

	pub fn get_texture<'b>(&'b mut self) -> &glium::Texture2d {
		// this is done in this if else structure instead of through a match because using a match causes reference scope issues.
		if self.texture.is_none() {
			match self.receiver.try_recv() {
				Ok(tex) => {
					self.texture = Some(glium::Texture2d::new(
						self.display, 
						glium::texture::RawImage2d::from_raw_rgba(tex.0, tex.1)).expect("failed to generate glium texture"));
					return &self.texture.as_ref().expect("Should not be possible! Value just assigned!");
				},
				Err(_) => {
					return &self.empty_texture;
				} 
			}	
		} else {
			return &self.texture.as_ref().expect("Should not be possible! `None` condition is already prepared for!");
		}
	}
}

// This struct stores and provides a texture that is loaded from the disk on a second thread. 
// For textures it is better to provide an empty texture than return a conditionally valid resource imo, so we return `empty_texture`
// when the code asks for the texture before it's ready
pub struct OffloadedCompressedTexture<'a> {
	empty_texture: glium::texture::compressed_texture2d::CompressedTexture2d,
	texture: Option<glium::texture::compressed_texture2d::CompressedTexture2d>,
	receiver: mpsc::Receiver<(Vec<u8>, (u32, u32), u32)>,
	display: &'a glium::Display
}
impl<'a> OffloadedCompressedTexture<'a> {
	// Start a new thread with an anonymous function that loads the texture and then returns a message to the internal message pipeline
	pub fn new(path: String, disp: &'a glium::Display, empty_texture: glium::texture::compressed_texture2d::CompressedTexture2d) -> OffloadedCompressedTexture<'a> {
		let (sender, receiver) = mpsc::channel();

		let ret = OffloadedCompressedTexture {
			texture: None,
			receiver,
			display: disp,
			empty_texture
		};
		thread::spawn(move || {
			let data = file_loading::load_texture_dds(&path[..]);
			sender.send(data).expect("Failed to send texture data to main thread!");
		});
		ret
	}

	pub fn get_texture<'b>(&'b mut self) -> &glium::texture::compressed_texture2d::CompressedTexture2d {
		// this is done in this if else structure instead of through a match because using a match causes reference scope issues.
		if self.texture.is_none() {
			match self.receiver.try_recv() {
				Ok((data, dimensions, mipmap_count)) => {
					self.texture = Some(glium::texture::compressed_texture2d::CompressedTexture2d::with_compressed_data(
						self.display, 
						&data[..], 
						dimensions.0,
						dimensions.1,
						glium::texture::CompressedFormat::S3tcDxt1NoAlpha,
						if mipmap_count > 1 {
							glium::texture::CompressedMipmapsOption::EmptyMipmaps
						} else {
							glium::texture::CompressedMipmapsOption::NoMipmap
						}
					).expect("failed to generate glium texture"));
					return &self.texture.as_ref().expect("Should not be possible! Value just assigned!");
				},
				Err(_) => {
					return &self.empty_texture;
				} 
			}	
		} else {
			return &self.texture.as_ref().expect("Should not be possible! `None` condition is already prepared for!");
		}
	}
}



pub trait Mesh<'a> {
	fn new(path: String) -> Self;
	fn get_data(&'a mut self, disp: &'a glium::Display) -> Option<(&'a glium::VertexBuffer<Vertex>, &'a glium::IndexBuffer<u32>)>;
}

pub struct OffloadedMesh { 
	vertex_buffer: Option<glium::VertexBuffer<Vertex>>,
	index_buffer: Option<glium::IndexBuffer<u32>>,

	receiver: mpsc::Receiver<(Vec<Vertex>, Vec<u32>)>,
}

impl<'a> Mesh<'a> for OffloadedMesh {
    fn new(path: String) -> Self {
		let (sender, receiver) = mpsc::channel();

		let temp_path = path.clone();
		thread::spawn(move || {
			let mut data = Vec::new();
			let mut index_buffer = Vec::new();

			file_loading::load_model(&temp_path[..], &mut data, &mut index_buffer);

			sender.send((data, index_buffer)).expect("Failed to send texture data to main thread!");
		});

		return OffloadedMesh {
			vertex_buffer: None,
			index_buffer: None,
			receiver
		};
    }
    fn get_data(&'a mut self, display: &'a glium::Display) -> Option<(&'a glium::VertexBuffer<Vertex>, &'a glium::IndexBuffer<u32>)> {
		// this is done in this if else structure instead of through a match because using a match causes reference scope issues.
		if self.vertex_buffer.is_none() {
			match self.receiver.try_recv() {
				Ok((vertex_data, index_data)) => {
					self.vertex_buffer = Some(glium::vertex::VertexBuffer::new(display, &vertex_data).expect("Failed to build vertex buffer"));
					self.index_buffer = Some(glium::index::IndexBuffer::new(
						display, glium::index::PrimitiveType::TrianglesList, &index_data).expect("Failed to build index buffer"));
					return Some((&self.vertex_buffer.as_ref().expect("Should not be possible! Value just assigned!"),
							  &self.index_buffer.as_ref().expect("Should not be possible! Value just assigned!")));
				},
				Err(_) => {
					return None;
				} 
			}	
		} else {
			return Some((&self.vertex_buffer.as_ref().expect("Should not be possible! `None` condition is already prepared for!"),
					  &self.index_buffer.as_ref().expect("Should not be possible! `None` condition is already prepared for!")));
		}
    }
	
}


pub trait MeshInstance<'a> {
	fn draw(
		&mut self, 
		frame: &mut glium::Frame, 
		projection_matrix: &Mat4, 
		view_matrix: &Mat4, 
		time: f32);
	fn new(path: String, disp: &'a glium::Display) -> Self;
}

pub trait PhysicsObject {
	fn get_position(&self) -> Vec3;
	fn get_rotation(&self) -> Quat;
	fn step_physics(&mut self, delta: f32);
}


// This struct stores and provides a model that is loaded from the disk on a second thread. 
// For models it is better to return conditional validity instead of a fake asset, so we return an Option when the code asks for the
// model before it's ready
pub struct PhysicsModel<'a> {
	mesh: OffloadedMesh,

	display: &'a glium::Display,
	program: glium::Program,
	draw_parameters: glium::DrawParameters<'a>,

	albedo_texture: OffloadedCompressedTexture<'a>, 
	normal_texture: OffloadedTexture<'a>, 

	position: Vec3,
	rotation: Vec3
}

impl<'a> MeshInstance<'a> for PhysicsModel<'a> {
	// Start a new thread with an anonymous function that loads the texture and then returns a message to the internal message pipeline
	fn new(path: String, disp: &'a glium::Display) -> PhysicsModel<'a> {

			// setting up the draw parameters - mostly uses the default, but we want to enable culling back faces and use the depth test
		let draw_parameters = glium::draw_parameters::DrawParameters {
		stencil: Default::default(),
		blend: Default::default(),
		color_mask: (true, true, true, true),
		line_width: None,
		point_size: None,
		polygon_mode: glium::PolygonMode::Fill,
		clip_planes_bitmask: 0,
		viewport: None,
		scissor: None,
		draw_primitives: true,
		samples_passed_query: None,
		time_elapsed_query: None,
		primitives_generated_query: None,
		transform_feedback_primitives_written_query: None,
		condition: None,
		transform_feedback: None,
		smooth: None,
		provoking_vertex: glium::draw_parameters::ProvokingVertex::LastVertex,
		primitive_bounding_box: (-1.0 .. 1.0, -1.0 .. 1.0, -1.0 .. 1.0, -1.0 .. 1.0),
		primitive_restart_index: false,
		// Non-default values start here
		backface_culling: glium::draw_parameters::BackfaceCullingMode::CullClockwise,
		multisampling: true,
		dithering: true,
		depth: glium::Depth {
			clamp: glium::draw_parameters::DepthClamp::NoClamp,
			write: true,
			test: glium::draw_parameters::DepthTest::IfLessOrEqual,
			range: (0.0, 1.0)
		}
		};

		let albedo_texture = OffloadedCompressedTexture::new(
			String::from("assets/textures/test.dds"), 
			disp, 
			glium::texture::compressed_texture2d::CompressedTexture2d::new(
				disp, 
				glium::texture::RawImage2d::from_raw_rgb(vec![0u8, 0u8, 0u8], (1, 1)))
				.expect("Failed to build empty texture")); 
	
		let normal_texture = OffloadedTexture::new(
			String::from("assets/textures/test_normal.png"), 
			disp, 
			glium::Texture2d::new(
				disp, 
				glium::texture::RawImage2d::from_raw_rgb(vec![128u8, 128u8, 255u8], (1, 1)))
				.expect("Failed to build empty texture"));
			
		let ret = PhysicsModel {
			mesh: OffloadedMesh::new(path),

			display: disp,
			program: file_loading::load_shaders(disp, "assets/shaders/vert.vs", "assets/shaders/frag.fs"),
			draw_parameters,

			albedo_texture,
			normal_texture,

			position: [0.0; 3],
			rotation: [0.0; 3]
		};
		ret
	}
	// Draws the object to the given framebuffer if the model is loaded.
	fn draw(
			&mut self, 
			frame: &mut glium::Frame, 
			projection_matrix: &Mat4, 
			view_matrix: &Mat4, 
			time: f32) {

		let data = self.mesh.get_data(self.display);
		let (v_buffer, i_buffer) = match data {
			Some((v_buffer, i_buffer)) => {
				(v_buffer, i_buffer)
			},
			None => {return;}
		};

		let model_matrix = get_transformation_matrix(
			&[1.0, 1.0, 1.0], 
			&[0.0, time.sin() / 10.0, 3.0], 
			&[0.0, time / 10.0, 0.0]);
		let mv_matrix = mat4::multiply(&mut Mat4::default(), &view_matrix, &model_matrix);

		let mvp_matrix = mat4::multiply(&mut Mat4::default(), 
			&projection_matrix,&mv_matrix);
		let mv3x3_matrix = mat3::from_mat4(&mut mat3::create(), &mv_matrix);
			// Since the vertex buffer and index buffer should never be populated without the other we're panicking if they don't match
			// the names of the fields in this anonymous struct need to match the field names in the shaders
		let gen_uniform = uniform! {
			model_matrix: convert_matrix(&model_matrix),
			view_matrix: convert_matrix(&view_matrix),
			projection_matrix: convert_matrix(&projection_matrix),
			mvp_matrix: convert_matrix(&mvp_matrix),
			mv3x3_matrix: convert_matrix_3x3(&mv3x3_matrix),
			time: time,
			albedo_texture: self.albedo_texture.get_texture(),
			normal_texture: self.normal_texture.get_texture(),
		};
		match frame.draw(
				v_buffer, 
				i_buffer, &self.program, 
				&gen_uniform, 
				&self.draw_parameters) {
		
			Ok(_) => (),
			Err(e) => {
				println!("[Object Draw] {:?}", e);
				panic!(e);
			}
		}

	}
}

impl<'a> PhysicsObject for PhysicsModel<'a> {
	fn get_position(&self) -> Vec3 {
		 self.position
	}
	fn get_rotation(&self) -> Quat {
		let mut q = quat::create();
		 quat::from_euler(&mut q, self.rotation[0], self.rotation[1], self.rotation[2]);
		 q
	}
	fn step_physics(&mut self, _delta: f32) {
		 //TODO:
	}
}


pub(crate) struct EnvironmentModel<'a> {
	mesh: OffloadedMesh,

	display: &'a glium::Display,
	program: glium::Program,
	draw_parameters: glium::DrawParameters<'a>,

	albedo_texture: OffloadedCompressedTexture<'a>, 
}

impl<'a> MeshInstance<'a> for EnvironmentModel<'a> {
	fn draw(
			&mut self, 
			frame: &mut glium::Frame, 
			projection_matrix: &Mat4, 
			view_matrix: &Mat4, 
			time: f32) {
		let data = self.mesh.get_data(self.display);
		let (v_buffer, i_buffer) = match data {
			Some((v_buffer, i_buffer)) => {
				(v_buffer, i_buffer)
			},
			None => {return;}
		};
	
		let model_matrix = get_transformation_matrix(
			&[1.0, 1.0, 1.0], 
			&[0.0, 0.0, 0.0], 
			&[0.0, 0.0, 0.0]);
		let mv_matrix = mat4::multiply(&mut Mat4::default(), &view_matrix, &model_matrix);
	
		let mvp_matrix = mat4::multiply(&mut Mat4::default(), 
			&projection_matrix,&mv_matrix);
		let mv3x3_matrix = mat3::from_mat4(&mut mat3::create(), &mv_matrix);
			// Since the vertex buffer and index buffer should never be populated without the other we're panicking if they don't match
			// the names of the fields in this anonymous struct need to match the field names in the shaders
		let gen_uniform = uniform! {
			model_matrix: convert_matrix(&model_matrix),
			view_matrix: convert_matrix(&view_matrix),
			projection_matrix: convert_matrix(&projection_matrix),
			mvp_matrix: convert_matrix(&mvp_matrix),
			mv3x3_matrix: convert_matrix_3x3(&mv3x3_matrix),
			time: time,
			albedo_texture: self.albedo_texture.get_texture()
		};
		match frame.draw(
				v_buffer, 
				i_buffer, &self.program, 
				&gen_uniform, 
				&self.draw_parameters) {
		
			Ok(_) => (),
			Err(e) => {
				println!("[Object Draw] {:?}", e);
				panic!(e);
			}
		}
	}
	fn new(path: String, disp: &'a glium::Display) -> Self {
		// setting up the draw parameters - mostly uses the default, but we want to enable culling back faces and use the depth test
		let draw_parameters = glium::draw_parameters::DrawParameters {
			stencil: Default::default(),
			blend: Default::default(),
			color_mask: (true, true, true, true),
			line_width: None,
			point_size: None,
			polygon_mode: glium::PolygonMode::Fill,
			clip_planes_bitmask: 0,
			viewport: None,
			scissor: None,
			draw_primitives: true,
			samples_passed_query: None,
			time_elapsed_query: None,
			primitives_generated_query: None,
			transform_feedback_primitives_written_query: None,
			condition: None,
			transform_feedback: None,
			smooth: None,
			provoking_vertex: glium::draw_parameters::ProvokingVertex::LastVertex,
			primitive_bounding_box: (-1.0 .. 1.0, -1.0 .. 1.0, -1.0 .. 1.0, -1.0 .. 1.0),
			primitive_restart_index: false,
			// Non-default values start here
			backface_culling: glium::draw_parameters::BackfaceCullingMode::CullClockwise,
			multisampling: true,
			dithering: true,
			depth: glium::Depth {
				clamp: glium::draw_parameters::DepthClamp::NoClamp,
				write: true,
				test: glium::draw_parameters::DepthTest::IfLessOrEqual,
				range: (0.0, 1.0)
			}
		};

		let albedo_texture = OffloadedCompressedTexture::new(
			String::from("assets/textures/hdr.dds"), 
			disp, 
			glium::texture::compressed_texture2d::CompressedTexture2d::new(
				disp, 
				glium::texture::RawImage2d::from_raw_rgb(vec![0u8, 0u8, 0u8], (1, 1)))
				.expect("Failed to build empty texture"));

		let ret = EnvironmentModel {
			mesh: OffloadedMesh::new(path),

			display: disp,
			program: file_loading::load_shaders(disp, "assets/shaders/vert.vs", "assets/shaders/frag_unshaded.fs"),
			draw_parameters,

			albedo_texture
		};
		ret
    }
	
} 