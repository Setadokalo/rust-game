use crate::common::Vertex;
use obj::{ObjData, IndexTuple};
use image::ImageDecoder;
use gl_matrix::{vec2, vec3};
use std::{thread, io::BufReader};

fn checked_add_vert(data: &mut Vec<Vertex>, index_array: &mut Vec<u32>, obj_data: &ObjData, tri_vertices: [&IndexTuple; 3]) {
	// vertex position shortcuts
	let v0 = obj_data.position[tri_vertices[0].0];
	let v1 = obj_data.position[tri_vertices[1].0];
	let v2 = obj_data.position[tri_vertices[2].0];
	// uv shortcuts
	
	let uv0 = match tri_vertices[0].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0]
	};
	let uv1 = match tri_vertices[1].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0]
	};
	let uv2 = match tri_vertices[2].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0]
	};

	let delta_pos_1 = vec3::subtract(&mut vec3::create(), &v1, &v0);
	let delta_pos_2 = vec3::subtract(&mut vec3::create(), &v2, &v0);

	let delta_uv_1 = vec2::subtract(&mut vec2::create(), &uv1, &uv0);
	let delta_uv_2 = vec2::subtract(&mut vec2::create(), &uv2, &uv0);
	let r = 1.0_f32 / (delta_uv_1[0] * delta_uv_2[1] - delta_uv_1[1] * delta_uv_2[0]);
	let tan = vec3::scale(&mut vec3::create(), &vec3::subtract(&mut vec3::create(), 
		&vec3::scale(&mut vec3::create(), &delta_pos_1, delta_uv_2[1]), 
		&vec3::scale(&mut vec3::create(), &delta_pos_2, delta_uv_1[1])), r);

	let bitan = vec3::scale(&mut vec3::create(), &vec3::subtract(&mut vec3::create(),
		&vec3::scale(&mut vec3::create(), &delta_pos_2, delta_uv_1[0]),
		&vec3::scale(&mut vec3::create(), &delta_pos_1, delta_uv_2[0])), r);

	let first_vert =Vertex::new(
		v0,
		uv0,
		// not having a normal value is probably undefined behaviour and should be handled as such... we'll see
		match tri_vertices[0].2 {
			Some(index) => obj_data.normal[index],
			None => [0.0, 1.0, 0.0]
		},
		tan,
		bitan
	);
	let second_vert =Vertex::new(
		v1,
		uv1,
		// not having a normal value is probably undefined behaviour and should be handled as such... we'll see
		match tri_vertices[1].2 {
			Some(index) => obj_data.normal[index],
			None => [0.0, 1.0, 0.0]
		},
		tan,
		bitan
	);
	let third_vert =Vertex::new(
		v2,
		uv2,
		// not having a normal value is probably undefined behaviour and should be handled as such... we'll see
		match tri_vertices[2].2 {
			Some(index) => obj_data.normal[index],
			None => [0.0, 1.0, 0.0]
		},
		tan,
		bitan
	);
	check_and_add_vert(data, index_array, first_vert);
	check_and_add_vert(data, index_array, second_vert);
	check_and_add_vert(data, index_array, third_vert);
}

fn check_and_add_vert(data: &mut Vec<Vertex>, index_array: &mut Vec<u32>, mut current_vert: Vertex) {
	// I doubt this is the most efficient way to do this, but from everything I tried this was the only way I got to work. TODO: try making this better
	// Using rposition instead of position because most repeat vertices will most likely be recently added
	// For reference - loading the Utah Teapot takes ~1.87 seconds using `position` and ~0.45 seconds using `rposition`
	let vert_index = match data.iter().rposition(|&v| v == current_vert) {
		Some(v) => v as i64,
		None => -1
	};
	if vert_index != -1 {
		// combine the tangent and bitangent values
		let temp = 
		vec3::add(&mut vec3::create(), &data.get(vert_index as usize).expect("failed to read vertex").tangent, &current_vert.tangent);
		current_vert.tangent = temp;
		let temp = 
		vec3::add(&mut vec3::create(), &data.get(vert_index as usize).expect("failed to read vertex").bitangent, &current_vert.bitangent);
		current_vert.bitangent = temp;
		data[vert_index as usize] = current_vert;
		index_array.push(vert_index as u32); // using -1 as the default index because otherwise there'd be confusion about what is and isn't a valid point - 
		// is Integer.MAX_VALUE the index of the vertex, or the value indicating no data?
	} else {
		data.push(current_vert);
		index_array.push((data.len() - 1) as u32);
	}
}

// also note that this code is still RIDDLED with `expect`s - it's VERY likely to crash the program if ANYTHING goes wrong at ALL
pub fn load_model(path: &str, data: &mut Vec<Vertex>, index_array: &mut Vec<u32>) {
	let test_object_result = obj::Obj::load(path);
	let test_object = match test_object_result {
		Ok(obj) => obj,
		Err(e) => panic!(e)
	};
	// it's almost definitely a problem if `data` is not empty so we're gonna double check here
	assert_eq!(data.len() % 3, 0);
	// for now, I'm only working under the assumption that there is EXACTLY ONE object in the model file.
	// eventually I'd like to expand this and make the conversion more robust
	assert_eq!(test_object.data.objects.len(), 1);

	let obj_object = match test_object.data.objects.get(0) {
		Some(obj) => obj,
		None => panic!("Failed to load obj internal object")
	};

	for obj_group in obj_object.groups.iter() {

		data.reserve(obj_group.polys.len() * 3);

		for poly in obj_group.polys.iter() {

			// using simple triangle fan triangulation - We're assuming all polygons are complex because FUCK this shit gets complicated
			for i in 2..poly.0.len() {
				let tri_start_vert = poly.0.get(0).expect("indexing error");
				let tri_mid_vert = poly.0.get(i - 1).expect("indexing error");
				let tri_last_vert = poly.0.get(i).expect("indexing error");
				checked_add_vert(data, index_array, &test_object.data, [tri_start_vert, tri_mid_vert, tri_last_vert]);
			}
		}
	}

}

pub fn load_texture(path: &str) -> (Vec<u8>, (u32, u32)) {
	if path.ends_with(".png") {
		return load_texture_png(path);
	} else if path.ends_with(".jpg") || path.ends_with(".jpeg") {
		return load_texture_jpg(path);
	} else if path.ends_with(".ff") || path.ends_with(".farbfeld") { //.farbfeld is technically invalid but I bet some people use it
		return load_texture_ff(path);
	} else {
		panic!("Invalid texture type!");
	}
}

pub fn load_texture_png(path: &str) -> (Vec<u8>, (u32, u32)) {
	let start_time = std::time::Instant::now();
	let dfile = std::fs::File::open(path).expect("failed to load data");
	// glium::Texture2d::new(facade, data).expect("Failed to generate texture")
	let img = image::png::PngDecoder::new(dfile).expect("Failed to load image file");
	let mut data: Vec<u8> = vec![0u8; img.total_bytes() as usize];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..]).expect("Failed to convert image to raw data");
	println!("[PNG Decoder] Returning constructed image; took {:.4} seconds", start_time.elapsed().as_secs_f64());
	(data, dimensions)
}

pub fn load_texture_dds(path: &str) -> (Vec<u8>, (u32, u32), u32) {
	let start_time = std::time::Instant::now();
	let dfile = std::fs::File::open(path).expect("failed to load data");
	let mut reader = BufReader::new(dfile);
	let mut dds = ddsfile::Dds::read(&mut reader).expect("Failed to read dds file");
	let dimensions = (dds.get_width(), dds.get_height());
	let data  = dds.get_data(0).expect("Failed to get layer data");
	// dds.read_image(&mut data[..]).expect("Failed to convert image to raw data");
	println!("[DDS Decoder] Returning constructed image; took {:.4} seconds", start_time.elapsed().as_secs_f64());
	println!("[DDS Decoder] Image info: Dimensions: {:?}   Format: {:?}", [dimensions.0, dimensions.1], dds.get_d3d_format());
	thread::sleep_ms(1000);
	// println!("[DDS Decoder] {:?}", data); 
	(Vec::from(data), dimensions, dds.get_num_mipmap_levels())
}

pub fn load_texture_jpg(path: &str) -> (Vec<u8>, (u32, u32)) {
	let start_time = std::time::Instant::now();
	let dfile = std::fs::File::open(path).expect("failed to load data");
	// glium::Texture2d::new(facade, data).expect("Failed to generate texture")
	let img = image::jpeg::JpegDecoder::new(dfile).expect("Failed to load image file");
	let mut data: Vec<u8> = vec![0u8; img.total_bytes() as usize];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..]).expect("Failed to convert image to raw data");
	println!("[JPEG Decoder] Returning constructed image; took {:.4} seconds", start_time.elapsed().as_secs_f64());
	(data, dimensions)
}

pub fn load_texture_ff(path: &str) -> (Vec<u8>, (u32, u32)) {
	let dfile = std::fs::File::open(path).expect("failed to load data");
	// glium::Texture2d::new(facade, data).expect("Failed to generate texture")
	let img = image::farbfeld::FarbfeldDecoder::new(dfile).expect("Failed to load image file");
	let mut data: Vec<u8> = vec![0u8; img.total_bytes() as usize];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..]).expect("Failed to convert image to raw data");
	(data, dimensions)
}

// pub fn convert_image_png_to_farbfeld<'a>(source_path: &'a str, dest_path: Option<&str>) {
// 	let mut stri: String;
// 	let dest_path = match dest_path {
// 		Some(dest) => dest,
// 		None => {
// 			// if no output specified, just take the source path, remove the .png, and append .ff
// 			stri = (source_path[..source_path.len()-4]).to_string();
// 			stri.push_str(".ff");
// 			&stri.as_str()
// 		}
// 	};
// 	let img_data = load_texture_png("test_normal.png");

// 	save_img_as_farbfeld(&img_data.0, dest_path, img_data.1);

// }


// pub fn save_img_as_farbfeld(data: &Vec<u8>, path: &str, dimensions: (u32, u32)) {
// 	let out = std::fs::File::create(path).expect("Failed to create output file");
// 	let encoder = image::farbfeld::FarbfeldEncoder::new(out);
// 	encoder.encode(data, dimensions.0, dimensions.1).expect("Failed to encode image");
// }

pub fn get_shader(path: &str) -> String {
	std::fs::read_to_string(path).expect(&format!("Failed to load shader \"{}\"", path)[..])
}


pub fn load_shaders(display: &glium::Display, vertex_shader_path: &str, fragment_shader_path: &str) -> glium::Program {
	// loads the vertex and fragment shader from the disk, then compiles them into `program`
	let vertex_shader_source = get_shader(vertex_shader_path);
	let fragment_shader_source = get_shader(fragment_shader_path);

	glium::Program::from_source(display, 
		&vertex_shader_source, 
		&fragment_shader_source, 
		None).expect("Failed to compile shader")
}
