#version 460
in vec3 position;
in vec2 uv;
in vec3 vert_normal;
in vec3 tangent;
in vec3 bitangent;

out vec3 normal;
out vec3 tangent_f;
out vec3 bitangent_f;
out vec3 model_normal;
out vec3 light_dir_tanspace;
out vec3 eye_dir_tanspace;
out vec2 uv_f;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 mvp_matrix;
uniform mat3 mv3x3_matrix;


void main() {
	// for now we're assuming light always comes from a fixed point
	vec3 light_pos = vec3(4.0, 10.0, 0.0);
	gl_Position = mvp_matrix * vec4(position, 1.0);

	vec3 normal_camspace = mv3x3_matrix * normalize(vert_normal);
	vec3 tangent_camspace = mv3x3_matrix * normalize(tangent);
	vec3 bitangent_camspace = mv3x3_matrix * normalize(bitangent);

	mat3 TBN = transpose(mat3(
		tangent_camspace,
		bitangent_camspace,
		normal_camspace
	));

	normal = (view_matrix * model_matrix * vec4(vert_normal, 0.0)).xyz;
	model_normal = vert_normal;

	vec3 vertex_pos_camspace = (view_matrix * model_matrix * vec4(position, 1)).xyz;
	vec3 eye_dir_camspace = vec3(0, 0, 0) - vertex_pos_camspace;
	eye_dir_tanspace = TBN * eye_dir_camspace;

	vec3 light_pos_camspace = (view_matrix * vec4(light_pos, 1.0)).xyz;
	vec3 light_dir_camspace = light_pos_camspace + eye_dir_camspace;
	light_dir_tanspace = TBN *  light_dir_camspace;
	
	uv_f = uv;
}
