
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct HashableVertex {
	pub position: [OrderedFloat<f32>; 3],
	pub uv: [OrderedFloat<f32>; 2],
	pub vert_normal: [OrderedFloat<f32>; 3]
}
impl HashableVertex {
	pub fn new(position_v: Vec3, uv_v: Vec2, normal_v: Vec3) -> HashableVertex {
		let position = [OrderedFloat(position_v[0]), OrderedFloat(position_v[1]), OrderedFloat(position_v[2])];
		let uv = [OrderedFloat(uv_v[0]), OrderedFloat(uv_v[1])];
		let normal = [OrderedFloat(normal_v[0]), OrderedFloat(normal_v[1]), OrderedFloat(normal_v[2])];

		HashableVertex {
			position,
			uv,
			vert_normal: normal
		}
	}
}
impl From<HashableVertex> for Vertex {
	fn from(h_vert: HashableVertex) -> Self {
		let position = [h_vert.position[0].into_inner(), h_vert.position[1].into_inner(), h_vert.position[2].into_inner()];
		let uv = [h_vert.uv[0].into_inner(), h_vert.uv[1].into_inner()];
		let vert_normal = [h_vert.vert_normal[0].into_inner(), h_vert.vert_normal[1].into_inner(), h_vert.vert_normal[2].into_inner()];
		Vertex {
			position,
			uv,
			vert_normal
		}
	}
}

impl From<Vertex> for HashableVertex {
	fn from(vert: Vertex) -> Self {
		let position = [OrderedFloat(vert.position[0]), OrderedFloat(vert.position[1]), OrderedFloat(vert.position[2])];
		let uv = [OrderedFloat(vert.uv[0]), OrderedFloat(vert.uv[1])];
		let vert_normal = [OrderedFloat(vert.vert_normal[0]), OrderedFloat(vert.vert_normal[1]), OrderedFloat(vert.vert_normal[2])];
		HashableVertex {
			position,
			uv,
			vert_normal
		}
	}
}
