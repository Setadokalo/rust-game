use std::time::Instant;

use glium::{self, glutin, Surface};
use glutin::event_loop::ControlFlow;
use glutin::event;
use gl_matrix::common::*;
use gl_matrix::{mat4, vec3};

mod tests;

mod file_loading;
mod common;
use common::{MeshInstance};

static mut STATIC_DISPLAY: Option<glium::Display> = None;


fn set_up_window() -> (glutin::event_loop::EventLoop<()>, &'static glium::Display) {
	// 1. The **winit::EventsLoop** for handling events.
	let events_loop = glutin::event_loop::EventLoop::new();
	// 2. Parameters for building the Window.
	let wb = glium::glutin::window::WindowBuilder::new()
		 .with_inner_size(glium::glutin::dpi::LogicalSize::new(1024.0, 768.0)) //default window size
		 .with_min_inner_size(glium::glutin::dpi::LogicalSize::new(400.0, 300.0)) //minimum window size
		 .with_title("Testing");
	// 3. Parameters for building the OpenGL context.
	let cb = glium::glutin::ContextBuilder::new()
		.with_multisampling(4)
		.with_srgb(false)
	;
	unsafe {	
		// 4. Build the Display with the given window and OpenGL context parameters and register the
		//    window with the events_loop.
		STATIC_DISPLAY = Some(glium::Display::new(wb, cb, &events_loop).expect("Failed to create the window."));
		return (events_loop, STATIC_DISPLAY.as_ref().unwrap());
	}
}

fn main() {
	let (events_loop, display) = set_up_window();

	display.gl_window().window().set_cursor_visible(false);
	display.gl_window().window().set_cursor_grab(true).expect("Failed to grab cursor");
	

	let start_time = Instant::now();

	let mut teapot = common::PhysicsModel::new(String::from("assets/models/newell_teaset/teapot_upgrade.obj"), display);

	let mut hdri_sphere = common::EnvironmentModel::new(
		String::from("assets/models/hdr_globe.obj"), 
		display);


	let clear_color = Some((0.2, 0.2, 0.2, 1.0));

	let mut camera_pos: Vec3 = [0.0, 0.0, -1.0];
	let mut camera_rot: Vec3 = [0.0;3];
	// magnitude to move the camera up and down
	let mut input_dir = [0.0; 3];
	let mut linear_speed = [0.0; 3];

	let mut last_second = 0.0;
	let mut last_frame: f32 = 0.0;
	let mut frames_run = 0;
	let mut frames_at_last_sec = 0;

	let mut aspect_ratio = 1024.0 / 768.0;
	let mut fullscreen = false;

	let mut mouse_delta = [0.0_f64; 2];

	events_loop.run(move |event, _target, control| {
		match event {
			event::Event::WindowEvent {event: e, ..} => {
				match e {
					event::WindowEvent::CloseRequested => {
						close_window(control);
					},
					event::WindowEvent::Resized(new_size) => {
						aspect_ratio = (new_size.width as f32) / (new_size.height as f32);
					},
					event::WindowEvent::KeyboardInput {input, ..} => {
						let keycode = input.scancode;
						// 57 is space, 29 is control
						match keycode {
							57 => {
								if input.state == event::ElementState::Pressed {
									input_dir[1] = -1.0;
								} else {
									input_dir[1] = 0.0;
								}
							},
							29 => {
								if input.state == event::ElementState::Pressed {
									input_dir[1] = 1.0;
								} else {
									input_dir[1] = 0.0;
								}
							},
							17 => {
								if input.state == event::ElementState::Pressed {
									input_dir[2] = -1.0;
								} else {
									input_dir[2] = 0.0;
								}
							},
							30 => {
								if input.state == event::ElementState::Pressed {
									input_dir[0] = 1.0;
								} else {
									input_dir[0] = 0.0;
								}
							},
							31 => {
								if input.state == event::ElementState::Pressed {
									input_dir[2] = 1.0;
								} else {
									input_dir[2] = 0.0;
								}
							},
							32 => {
								if input.state == event::ElementState::Pressed {
									input_dir[0] = -1.0;
								} else {
									input_dir[0] = 0.0;
								}
							},
							87 => {
								if input.state == event::ElementState::Pressed {
									if !fullscreen {
										display.gl_window().window().set_fullscreen(Some(glutin::window::Fullscreen::Borderless(
												display.gl_window().window().current_monitor())));
									} else {
										display.gl_window().window().set_fullscreen(None);
									}
									fullscreen = !fullscreen;
								}
							},
							1 => {
								close_window(control);
							},
							_ => {println!("[Main] User pressed unknown key \"{}\"", keycode)}
						}
					},
					_ => {}
				}
			}
			glutin::event::Event::DeviceEvent {event, ..} => {
				match event {
					glutin::event::DeviceEvent::MouseMotion {delta} => {
						mouse_delta[0] += delta.0;
						mouse_delta[1] += delta.1;
					}
					_ => {}
				}
			}
			// No remaining events to process - return to rendering
			glutin::event::Event::MainEventsCleared => {

				let mut frame = display.draw();
				frame.clear(None, clear_color, true, Some(std::f32::MAX), None);
				// Create a transformation matrix that's just the unit matrix for now

				let seconds_since_start = start_time.elapsed().as_secs_f32();
				if seconds_since_start > last_second + 2.0 {
					println!("[Main] {} FPS at {:.7} ms/frame; {} seconds passed", 
						(frames_run - frames_at_last_sec) / 2, 
						(seconds_since_start - last_second) / ((frames_run - frames_at_last_sec) as f32),
						seconds_since_start - last_second);
					frames_at_last_sec = frames_run;
					last_second = seconds_since_start;
				}
				frames_run += 1;
				let delta = seconds_since_start - last_frame;
				last_frame = seconds_since_start;

				let time = seconds_since_start;

				// camera_pos[0] += delta * move_dir[0];
				camera_pos[1] += delta * input_dir[1];
				// camera_pos[2] += delta * move_dir[2];

				camera_rot[1] -= (mouse_delta[0] * 0.005) as f32;
				camera_rot[0] -= (mouse_delta[1] * 0.003) as f32;

				// the directions the player is actually inputting in world space
				// note that we're ignoring the player's pitch and only using their heading - most input ignores the pitch component
				let temp_vec = &vec3::rotate_y(&mut vec3::create(), &input_dir, &vec3::create(), -camera_rot[1]);
				linear_speed = vec3::add(&mut vec3::create(),&linear_speed, &vec3::scale(&mut vec3::create(), &temp_vec, delta * 0.015));
				linear_speed = vec3::scale(&mut vec3::create(), &linear_speed, 0.9975);

				camera_pos = vec3::sub(&mut vec3::create(), &camera_pos, &linear_speed);

				mouse_delta = [0.0; 2];

				let view_matrix = common::get_camera_transformation_matrix(&camera_pos, &camera_rot);

				let mut projection_matrix = mat4::create();
				mat4::perspective(&mut projection_matrix, (-90.0_f32).to_radians(), aspect_ratio, 0.01, Some(1000.0));			
				
				teapot.draw(
					&mut frame, 
					&projection_matrix, 
					&view_matrix, 
					time
				);

				//drawing the hdri last because it should always be the background
				hdri_sphere.draw(
					&mut frame, 
					&projection_matrix, 
					&view_matrix, 
					time
				);

				match frame.finish() {
					Ok(_) => (),
					Err(e) => panic!(e)
				}
			},
			_ => {}
		}
		
	});
}

fn close_window(control: &mut ControlFlow) {
	*control = ControlFlow::Exit;
	println!("[Main] Closing Program!");
}