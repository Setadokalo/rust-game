#version 330 core

in vec2 uv_f;
in vec3 normal;
in vec3 model_normal;

out vec4 color;

uniform float time;
uniform sampler2D albedo_texture;


void main() {
	vec2 uv = uv_f;
	vec3 albedo = texture(albedo_texture, uv).rgb; 
	// temporary workaround until I get unlazy enough to actually change the compressedtextures to use sRGB
	color = vec4(pow(albedo, vec3(2.2)), 1.0);
}